#include <iostream>
#include <cmath>
#include "Vector.h"
#include <vector>


	Vector::Vector() : x(0), y(0), z(0)
	{
	}

	Vector::Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{
	}

	void Vector::Show()
	{
		std::cout << "\n" << x << " " << y << " " << z;
	}

	double Vector::Module()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
    int main()
    {
        using namespace std;
        vector<int> v1, v2, v3;

        v1.push_back(10);
        v1.push_back(20);
        v1.push_back(30);
        v1.push_back(40);
        v1.push_back(50);

        std:: cout << "v1 = ";
        for (auto& v : v1) {
        std::cout << v << " ";
        }
        std::cout << "\n";

        v2.assign(v1.begin(), v1.end());
        std::cout << "v2 = ";
        for (auto& v : v2) {
            std::cout << v << " ";
        }
        std::cout << "\n";

        v3.assign(7, 4);
        std::cout << "v3 = ";
        for (auto& v : v3) {
            std::cout << v << " ";
        }
        std::cout << "\n";

        v3.assign({ 5, 6, 7 });
        for (auto& v : v3) {
            std::cout << v << " ";
        }
        std::cout << "\n";
    }